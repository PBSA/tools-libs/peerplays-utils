# SON-QA-Reference.xlsx, sheet "SON-for-Hive"

import pytest

from string_utils import *
from pp_utils import *
from hive_utils import *

ACCOUNT_PP = "account02"
ACCOUNT_PP2 = "account551" + get_rerun_toleration_suffix()
ACCOUNT_HIVE = "hive55" + get_rerun_toleration_suffix()


# SON-242: Peerplays Account to Hive deposit address 1:1 Mapping - Add
def test_hive_deposit_address_add():
    # Create new PP account
    acc_pp = pp_create_new_account(BRAIN_KEY, ACCOUNT_PP2, "nathan", "nathan")

    # Create new Hive account
    key_owner = hive_suggest_brain_key()["pub_key"]
    key_active = hive_suggest_brain_key()["pub_key"]
    key_posting = hive_suggest_brain_key()["pub_key"]
    key_memo = hive_suggest_brain_key()["pub_key"]
    hive_create_account_with_keys(
        "initminer",
        ACCOUNT_HIVE,
        "null",
        key_owner,
        key_active,
        key_posting,
        key_memo)

    # Wait for changes to actualize
    time.sleep(5)

    # Add Hive sidechain address to PP account, should succeed
    result = pp_add_sidechain_address(
        ACCOUNT_PP2,
        "hive",
        key_active,
        ACCOUNT_HIVE,
        key_active,
        ACCOUNT_HIVE)
    # ensure succeeded
    assert result.get("ref_block_num") is not None and result.get(
        "error") is None

    # Guard delay
    time.sleep(5)

    # Create another PP account
    account_pp2 = "account553" + get_rerun_toleration_suffix()
    acc_pp2 = pp_create_new_account(BRAIN_KEY, account_pp2, "nathan", "nathan")

    # Wait for changes to actualize
    time.sleep(5)

    # Try to add the same Hive sidechain to another PP account, should fail
    result = pp_add_sidechain_address(
        account_pp2,
        "hive",
        key_active,
        ACCOUNT_HIVE,
        key_active,
        ACCOUNT_HIVE)
    # ensure failed
    assert result.get("ref_block_num") is None and result.get(
        "error") is not None
    # ensure failed with an appropriate exception
    assert result["error"].find("active deposit key already exists") != -1


# SON-262: Peerplays Account to Hive deposit address 1:1 Mapping - Delete
def test_hive_deposit_address_delete():
    # Get initial sidechain addresses
    addrs = pp_get_sidechain_addresses_by_account(ACCOUNT_PP2)
    # there must be at least one Hive address
    assert len([addr for addr in addrs if addr["sidechain"] == "hive"]) > 0

    # Delete all Hive sidechain addresses forom the account
    pp_delete_sidechain_address(ACCOUNT_PP2, "hive")

    # Get sidechain addresses again
    addrs = pp_get_sidechain_addresses_by_account(ACCOUNT_PP2)
    # no Hive addresses anymore
    assert len([addr for addr in addrs if addr["sidechain"] == "hive"]) == 0


# SON-272: Verify Hive Deposit
def test_hive_deposit():
    # Setup deposit parameters
    deposit_amount_HIVE = 100
    deposit_amount_HBD = 200

    # Find out SON-related addresses
    sidechain_deposit_addr = pp_get_sidechain_address_by_account_and_sidechain(
        ACCOUNT_PP, "hive")["deposit_address"]

    # Save initial balances
    pp_balance_HIVE_0 = pp_get_account_balance(ACCOUNT_PP, "HIVE")
    pp_balance_HBD_0 = pp_get_account_balance(ACCOUNT_PP, "HBD")
    hive_balance_HIVE_0 = hive_get_balance(sidechain_deposit_addr)
    hive_balance_HBD_0 = hive_get_hbd_balance(sidechain_deposit_addr)
    hive_son_balance_HIVE_0 = hive_get_balance("son-account")
    hive_son_balance_HBD_0 = hive_get_hbd_balance("son-account")

    # Make the deposit itself
    hive_transfer(
        sidechain_deposit_addr,
        "son-account",
        deposit_amount_HIVE,
        "TESTS",
        ACCOUNT_PP)
    hive_transfer(
        sidechain_deposit_addr,
        "son-account",
        deposit_amount_HBD,
        "TBD",
        ACCOUNT_PP)

    # Wait for completion
    pp_wait_for_operations_complete()
    time.sleep(5)

    # Get new balances
    pp_balance_HIVE_1 = pp_get_account_balance(ACCOUNT_PP, "HIVE")
    pp_balance_HBD_1 = pp_get_account_balance(ACCOUNT_PP, "HBD")
    hive_balance_HIVE_1 = hive_get_balance(sidechain_deposit_addr)
    hive_balance_HBD_1 = hive_get_hbd_balance(sidechain_deposit_addr)
    hive_son_balance_HIVE_1 = hive_get_balance("son-account")
    hive_son_balance_HBD_1 = hive_get_hbd_balance("son-account")

    # Verify proper balance changes
    assert pp_balance_HIVE_1 * HIVE_PRECISION == pp_balance_HIVE_0 * \
        HIVE_PRECISION + deposit_amount_HIVE * HIVE_PRECISION
    assert pp_balance_HBD_1 * HIVE_PRECISION == pp_balance_HBD_0 * \
        HIVE_PRECISION + deposit_amount_HBD * HIVE_PRECISION
    assert hive_balance_HIVE_1 * HIVE_PRECISION == hive_balance_HIVE_0 * \
        HIVE_PRECISION - deposit_amount_HIVE * HIVE_PRECISION
    assert hive_balance_HBD_1 * HIVE_PRECISION == hive_balance_HBD_0 * \
        HIVE_PRECISION - deposit_amount_HBD * HIVE_PRECISION
    assert hive_son_balance_HIVE_1 * HIVE_PRECISION == hive_son_balance_HIVE_0 * \
        HIVE_PRECISION + deposit_amount_HIVE * HIVE_PRECISION
    assert hive_son_balance_HBD_1 * HIVE_PRECISION == hive_son_balance_HBD_0 * \
        HIVE_PRECISION + deposit_amount_HBD * HIVE_PRECISION


# SON-302: Verify Hive Withdrawal
def test_hive_withdrawal():
    # Setup withdrawal parameters
    withdraw_amount_HIVE = 50
    withdraw_amount_HBD = 100

    # Find out SON-related addresses
    sidechain_withdraw_addr = pp_get_sidechain_address_by_account_and_sidechain(
        ACCOUNT_PP, "hive")["withdraw_address"]

    # Save initial balances
    pp_balance_HIVE_0 = pp_get_account_balance(ACCOUNT_PP, "HIVE")
    pp_balance_HBD_0 = pp_get_account_balance(ACCOUNT_PP, "HBD")
    hive_balance_HIVE_0 = hive_get_balance(sidechain_withdraw_addr)
    hive_balance_HBD_0 = hive_get_hbd_balance(sidechain_withdraw_addr)
    hive_son_balance_HIVE_0 = hive_get_balance("son-account")
    hive_son_balance_HBD_0 = hive_get_hbd_balance("son-account")

    # Make the withrdawal itself
    pp_transfer(ACCOUNT_PP, "son-account", withdraw_amount_HIVE, "HIVE")
    pp_transfer(ACCOUNT_PP, "son-account", withdraw_amount_HBD, "HBD")

    # Wait for completion
    pp_wait_for_operations_complete()
    time.sleep(15)

    # Get new balances
    pp_balance_HIVE_1 = pp_get_account_balance(ACCOUNT_PP, "HIVE")
    pp_balance_HBD_1 = pp_get_account_balance(ACCOUNT_PP, "HBD")
    hive_balance_HIVE_1 = hive_get_balance(sidechain_withdraw_addr)
    hive_balance_HBD_1 = hive_get_hbd_balance(sidechain_withdraw_addr)
    hive_son_balance_HIVE_1 = hive_get_balance("son-account")
    hive_son_balance_HBD_1 = hive_get_hbd_balance("son-account")

    # Verify proper balance changes
    assert pp_balance_HIVE_1 * HIVE_PRECISION == pp_balance_HIVE_0 * \
        HIVE_PRECISION - withdraw_amount_HIVE * HIVE_PRECISION
    assert pp_balance_HBD_1 * HIVE_PRECISION == pp_balance_HBD_0 * \
        HIVE_PRECISION - withdraw_amount_HBD * HIVE_PRECISION
    assert hive_balance_HIVE_1 * HIVE_PRECISION == hive_balance_HIVE_0 * \
        HIVE_PRECISION + withdraw_amount_HIVE * HIVE_PRECISION
    assert hive_balance_HBD_1 * HIVE_PRECISION == hive_balance_HBD_0 * \
        HIVE_PRECISION + withdraw_amount_HBD * HIVE_PRECISION
    assert hive_son_balance_HIVE_1 * HIVE_PRECISION == hive_son_balance_HIVE_0 * \
        HIVE_PRECISION - withdraw_amount_HIVE * HIVE_PRECISION
    assert hive_son_balance_HBD_1 * HIVE_PRECISION == hive_son_balance_HBD_0 * \
        HIVE_PRECISION - withdraw_amount_HBD * HIVE_PRECISION
