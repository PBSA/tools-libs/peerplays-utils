import pytest

from pp_utils import *

ACCOUNT_FROM = "account09"
ACCOUNT_TO = "account07"
TRANSFER_AMOUNT = 500000


def test_tranfer_from_to():
    start_amount_from = pp_list_account_balances(ACCOUNT_FROM)["TEST"]
    start_amount_to = pp_list_account_balances(ACCOUNT_TO)["TEST"]

    pp_transfer(ACCOUNT_FROM, ACCOUNT_TO, TRANSFER_AMOUNT, "TEST", "pytest")

    end_amount_from = pp_list_account_balances(ACCOUNT_FROM)["TEST"]
    end_amount_to = pp_list_account_balances(ACCOUNT_TO)["TEST"]

    assert end_amount_to == start_amount_to + TRANSFER_AMOUNT
    assert end_amount_from == start_amount_from - TRANSFER_AMOUNT
