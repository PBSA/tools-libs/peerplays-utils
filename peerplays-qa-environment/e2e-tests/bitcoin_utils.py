#!/usr/bin/python3

import json
import docker

from docker_utils import *
from json_utils import *
from string_utils import *

BTC_PRECISION = 10 ** 8
BTC_GENERATION_ADDR = "2NBynoqwHvHDXh4aRjTUxBRndPUKzg4UFDG"
BTC_TRANSCTION_FEE = 0.0002

BTC_DOCKER_CONTAINER = None


def send_btc_command(
        _command,
        _convert_response_to_json=True,
        _wallet=None):
    if _wallet is None:
        _wallet = "default"

    global BTC_DOCKER_CONTAINER
    if BTC_DOCKER_CONTAINER is None:
        BTC_DOCKER_CONTAINER = get_docker_container_by_image_name(
            BTC_DOCKER_IMAGE_NAME)

    cmd = './bitcoin-cli -rpcuser=1 -rpcpassword=1 -rpcwallet="{}" {}'.format(
        _wallet, _command)
    exit_code, byte_output = BTC_DOCKER_CONTAINER.exec_run(cmd)
    resp = byte_output.decode("utf-8").strip()
    print(CMD_LOG_PREFIX + "Bitcoin command:\n{}\n-->\n{}\n".format(cmd, resp))
    if _convert_response_to_json:
        return convert_string_to_json(resp)
    else:
        return resp


def btc_getnewaddress():
    return send_btc_command("getnewaddress", False)


def btc_getaddressinfo(_address):
    return send_btc_command("getaddressinfo {}".format(_address))


def btc_dumpprivkey(_address):
    return send_btc_command("dumpprivkey {}".format(_address), False)


def btc_getblockchaininfo():
    return send_btc_command("getblockchaininfo")


def btc_getblockcount():
    return send_btc_command("getblockcount", False)


def btc_getmempoolinfo():
    return send_btc_command("getmempoolinfo")


def btc_getmininginfo():
    return send_btc_command("getmininginfo")


def btc_getnetworkinfo():
    return send_btc_command("getnetworkinfo")


def btc_getpeerinfo():
    return send_btc_command("getpeerinfo")


def btc_estimatesmartfee():
    return send_btc_command("estimatesmartfee 100")


def btc_getbalance():
    return send_btc_command("getbalance", False)


def btc_getbalances():
    return send_btc_command("getbalances")


def btc_getunconfirmedbalance():
    return send_btc_command("getunconfirmedbalance")


def btc_listwallets():
    return send_btc_command("listwallets")


def btc_listunspent(
        _wallet,
        _addresses,
        _min_conf=None,
        _max_conf=None,
        _options=""):
    if _min_conf is None:
        _min_conf = 1
    if _max_conf is None:
        _max_conf = 9999999
    return send_btc_command("listunspent {} {} \"{}\" true {}".format(
        _min_conf,
        _max_conf,
        json.dumps(_addresses).replace(
            '"',
            '\\"'),
        _options), True, _wallet)


def btc_listunspent_per_address(
        _address,
        _wallet=None,
        _min_conf=None,
        _max_conf=None,
        _min_amount=None,
        _max_amount=None):
    options = {}
    if _min_amount is not None:
        options["minimumAmount"] = _min_amount
    if _max_amount is not None:
        options["maximumAmount"] = _max_amount
    return btc_listunspent(
        _wallet,
        [_address],
        _min_conf,
        _max_conf,
        "'" +
        json.dumps(options) +
        "'" if options else "")


def btc_listunspent_amount_per_address(_address, _wallet=None):
    return sum(unspent["amount"]
               for unspent in btc_listunspent_per_address(_address, _wallet))


def btc_sendtoaddress(_to, _amount):
    return send_btc_command(
        'sendtoaddress {} {} "" "" false'.format(
            _to, _amount), False)


def btc_generatetoaddress(_address, _nblocks=1):
    return send_btc_command(
        'generatetoaddress {} "{}"'.format(
            _nblocks, _address), False)


def btc_estimatesmartfee(_conf_target=1):
    json = send_btc_command("estimatesmartfee {}".format(_conf_target))
    return json.get("feerate", BTC_TRANSCTION_FEE)
