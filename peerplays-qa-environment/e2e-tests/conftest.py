
import string_utils


def pytest_addoption(parser):
    parser.addoption(
        '--tolerate-rerun',
        action='store_true',
        default=False,
        help='Add random suffixes to names of test-created accounts and other entitites, and thus enable re-running tests w/o QA env. restart. Use only internally, to debug tests themselves.')


def pytest_configure(config):
    string_utils.tolerate_rerun = config.getoption('--tolerate-rerun')
