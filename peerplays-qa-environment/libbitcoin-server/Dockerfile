FROM ubuntu-for-peerplays:latest
MAINTAINER Peerplays Blockchain Standards Association

RUN \
    apt-get update -y && \
      DEBIAN_FRONTEND=noninteractive apt-get install -y \
      libpcre3-dev libsodium-dev

WORKDIR /home/peerplays

RUN \
    mkdir src

#setup gsl needed for libbitcoin server
RUN \
    cd src && \
    git clone https://github.com/imatix/gsl.git && \
    cd gsl/src && \
    make -j$(nproc) && \
    sudo make install && \
    ldconfig

# Setup libbitcoin build
RUN \
    cd src && \
    git clone --branch version3.8.0 --depth 1 https://gitlab.com/PBSA/peerplays-1.0/libbitcoin-server.git

# Setup libbitcoin server build
RUN \
    cd src/libbitcoin-server && \
    ./install.sh --prefix=/usr/local/ --build-boost --build-zmq --disable-shared && \
    ldconfig

WORKDIR /home/peerplays/libbitcoinserver-network

# Setup libbitcoinserver runimage
RUN \
    mv ../src/libbitcoin-server/build-libbitcoin-server/libbitcoin-server/console/bs ./

ADD regtest.conf ./regtest.conf
ADD runserver.sh ./runserver.sh

RUN chown peerplays:root -R /home/peerplays/libbitcoinserver-network

# libbitcoin server TCP
EXPOSE 9091
# libbitcoin ZMQ port
EXPOSE 9093

# Run libbitcoin server
CMD ["/bin/sh" , "runserver.sh"]
